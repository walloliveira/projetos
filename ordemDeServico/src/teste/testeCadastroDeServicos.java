package teste;

import static org.junit.Assert.*;

import org.junit.Test;

import entity.Servico;

public class testeCadastroDeServicos {

	@Test
	public void testServicos() {
		Servico sv = new Servico("Instalacao de servidor de Arquivos - Samba");
		assertEquals("Instalacao de servidor de Arquivos - Samba", sv.getNome());
	}
}
